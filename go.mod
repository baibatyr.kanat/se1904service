module gitlab.com/tleuzhan13/se1904Service

go 1.16

require (
	github.com/ardanlabs/conf v1.4.0
	github.com/pkg/errors v0.9.1
)
